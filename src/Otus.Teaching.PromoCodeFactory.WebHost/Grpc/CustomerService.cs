using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Grpc;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Grpc.Services
{
    public class CustomerService : GrpcCustomer.GrpcCustomerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        public override async  Task<GrpcCustomerShortResponseList> GetCustomersAsync(Empty request, ServerCallContext context)
        {
            var customers =  await _customerRepository.GetAllAsync();

            var response = new  GrpcCustomerShortResponseList();

            var mappedCustomers =  customers.Select(x => new GrpcCustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
            response.Customers.AddRange(mappedCustomers);

            return response;
        }

        public override async Task<GrpcCustomerResponse> GetCustomerAsync(GrpcGetCustomerByIdRequest request, ServerCallContext context)
        {
            var customer =  await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
            {
                var status = new Status(StatusCode.NotFound, "Customer not found");
                throw new RpcException(status);
            }
            
            var response = new GrpcCustomerResponse
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };
            var responsePreference = customer.Preferences.Select(p =>
                new GrpcPreferenceResponse{
                    Id = p.PreferenceId.ToString(),
                    Name = p.Preference.Name
                }
            );
            response.Preferences.AddRange(responsePreference);
            return response;
        }

        public override async Task<GrpcCustomerResponse> CreateCustomerAsync(GrpcCreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var preferenceIdsGuidList = request.PreferenceIds.Select(p => Guid.Parse(p)).ToList();

            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(preferenceIdsGuidList);

            var newCustomer = new Customer();
            newCustomer.Id = Guid.NewGuid();
            
            newCustomer.FirstName = request.FirstName;
            newCustomer.LastName = request.LastName;
            newCustomer.Email = request.Email;

            newCustomer.Preferences = preferences.Select(p => new CustomerPreference()
            {
                CustomerId = newCustomer.Id,
                Preference = p,
                PreferenceId = p.Id
            }).ToList();
            
            await _customerRepository.AddAsync(newCustomer);

            var response = new GrpcCustomerResponse
            {
                Id = newCustomer.Id.ToString(),
                FirstName = newCustomer.FirstName,
                LastName = newCustomer.LastName,
                Email = newCustomer.Email
            };

            return response;
        }

        public override async Task<Empty> EditCustomersAsync(GrpcCreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            
            if (customer == null)
            {
                var status = new Status(StatusCode.NotFound, "Customer not found");
                throw new RpcException(status);
            }
            
            var preferenceIdsGuidList = request.PreferenceIds.Select(p => Guid.Parse(p)).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIdsGuidList);
            
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences.Select(p => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = p,
                PreferenceId = p.Id
            }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomerAsync(GrpcDeleteCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            
            if (customer == null)
            {
                var status = new Status(StatusCode.NotFound, "Customer not found");
                throw new RpcException(status);
            }

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}